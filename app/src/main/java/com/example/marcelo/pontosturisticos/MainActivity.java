package com.example.marcelo.pontosturisticos;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String[] titles;
    String[] descriptions;
    TypedArray photos;
    ListView listView;
    Resources resources;
    Context context;
    View view;
    FrameLayout image;
    TextView title;
    TextView description;
    TextView txtValorTotal;
    CheckBox checkBoxes[];
    String isChecked[];
    Double valorTotal;
    Double valorPasseio;

    Double valorAlmoco;
    Double valorAlmocoPadrao;
    SharedPreferences sharedPreferences;

    SharedPreferences.OnSharedPreferenceChangeListener spChanged = new
            SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                    GerarTotal();
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(spChanged);

        AtracaoesAdapter atracaoesAdapter = new AtracaoesAdapter();

        initializeFields();
        listView.setAdapter(atracaoesAdapter);
        listView.setOnItemClickListener(atracaoesAdapter);

    }

    private void initializeFields() {
        listView = (ListView) findViewById(R.id.listView);
        context = getApplicationContext();
        resources = context.getResources();
        titles = resources.getStringArray(R.array.tituloAtracoes);
        descriptions = resources.getStringArray(R.array.descricaoAtracoes);
        photos = resources.obtainTypedArray(R.array.fotoAtracoes);
        checkBoxes = new CheckBox[titles.length];
        isChecked = new String[titles.length];

        for (int i = 0; i < titles.length; i++) {
            isChecked[i] = "false";
        }

        valorTotal = 0.00;
        valorPasseio = 0.00;
        valorAlmocoPadrao = 30.00;

        txtValorTotal = (TextView) findViewById(R.id.txtValor);
        txtValorTotal.setText(String.format("R$ %.2f", valorTotal));
    }

    public void OpenPref(View view) {
        Intent intent = new Intent(MainActivity.this, PrefActivity.class);
        startActivityForResult(intent, 1);
    }

    public void ClickComprar(View view) {
        Comprar();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArray("CHECKBOXES", isChecked);
        outState.putDouble("VALORPASSEIO", valorPasseio);
        outState.putDouble("VALORTOTAL", valorTotal);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle saveInstanceState) {
        valorPasseio = saveInstanceState.getDouble("VALORPASSEIO");
        valorTotal = saveInstanceState.getDouble("VALORTOTAL");
        isChecked = saveInstanceState.getStringArray("CHECKBOXES");
        txtValorTotal.setText(String.format("R$ %.2f", valorTotal));
    }

    private class AtracaoesAdapter extends BaseAdapter implements AdapterView.OnItemClickListener  {

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public Object getItem(int position) {
            return titles[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater layoutInflater;

            layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

            view = layoutInflater.inflate(R.layout.list_row, null);

            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            if (isChecked[position].equals("true")) {
                checkBox.toggle();
            }

            image = (FrameLayout) view.findViewById(R.id.imgFrameLayout);
            image.setBackground(photos.getDrawable(position));
            title = (TextView) view.findViewById(R.id.titleView);
            title.setText(titles[position]);
            description = (TextView) view.findViewById(R.id.descriptionView);
            description.setText(descriptions[position]);

            return view;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            checkBox.toggle();
            Boolean bool = checkBox.isChecked();
            AtualizaValor(bool);
            isChecked[position] = bool.toString();

        }
    }

    private void AtualizaValor(Boolean checked) {

        Double valorPasseioPadrao = 15.00;

        if (checked) {
            valorPasseio += valorPasseioPadrao;
        } else {
            valorPasseio -= valorPasseioPadrao;
        }

        GerarTotal();
    }

    private void GerarTotal() {
        valorAlmoco = 0.00;

        Boolean almoco = sharedPreferences.getBoolean("prefAlmoço", false);
        String txtacompanhantes = sharedPreferences.getString("prefNunAcompanhantes", "0");
        Integer acompanhantes;

        if (!txtacompanhantes.isEmpty()) {
            acompanhantes = Integer.parseInt(txtacompanhantes);
        } else {
            acompanhantes = 0;
        }

        if (almoco && valorPasseio != 0) {
            valorAlmoco = valorAlmocoPadrao * (acompanhantes + 1);
        }

        valorTotal = valorPasseio * (acompanhantes + 1) + valorAlmoco;

        txtValorTotal.setText(String.format("R$ %.2f", valorTotal));
    }

    private void Comprar() {
        String nomeUsuario = sharedPreferences.getString("prefNomeUsuario", "");
        String formaPag = sharedPreferences.getString("prefFormaPagamento", "");

        String nomeUsuarioError = (String) getResources().getString(R.string.nomeUsuarioError);
        String formaPagError = (String) getResources().getString(R.string.formaPagError);
        String valorTotalError = (String) getResources().getString(R.string.valorTotalError);

        if (nomeUsuario.isEmpty()) {
            Toast.makeText(getApplicationContext(), nomeUsuarioError, Toast.LENGTH_LONG).show();
            return;
        }

        if (formaPag.isEmpty()) {
            Toast.makeText(getApplicationContext(), formaPagError, Toast.LENGTH_LONG).show();
            return;
        }

        if (valorTotal == 0) {
            Toast.makeText(getApplicationContext(), valorTotalError, Toast.LENGTH_LONG).show();
            return;
        }

        String message = createMessage(nomeUsuario);

        switch (formaPag) {
            case "WhatsApp":
                sendWhatsApp(message);
                break;

            case "E-mail":
                sendEmail(message);
                break;

            case "SMS":
                sendSMS(message);
                break;
        }

    }

    public void sendWhatsApp(String text){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("text/plain");
        intent.setPackage("com.whatsapp");
        if(intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }
    }

    public void sendEmail(String text){
        if(text.length() > 0){
            String address = "luis.trevisan@up.edu.br";
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto: "+address));
            intent.putExtra(Intent.EXTRA_SUBJECT, "Compra - Turismo Curitiba");
            intent.putExtra(Intent.EXTRA_TEXT, text);
            if(intent.resolveActivity(getPackageManager()) != null){
                startActivity(intent);
            }
        }
    }

    public void sendSMS(String text){
        if(text.length() > 0){
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setType("*/*");
            intent.setData(Uri.parse("smsto:999147696"));
            intent.putExtra("sms_body", text);
            intent.putExtra(Intent.EXTRA_STREAM,"");
            if(intent.resolveActivity(getPackageManager()) != null){
                startActivity(intent);
            }
        }
    }

    public String createMessage(String nomeUsuario) {
        String txtacompanhantes = sharedPreferences.getString("prefNunAcompanhantes", "0");
        Boolean almoco = sharedPreferences.getBoolean("prefAlmoço", false);
        String message = String.format("Olá meu nome é %s e desejo conhecer os seguintes lugares: ", nomeUsuario);
        String atracao = "";


        for (int i = 0; i < titles.length; i++) {
            if (isChecked[i].equals("true")) {
                atracao += String.format(" %s", titles[i]);
            }
        }

        message += atracao + ". ";


        if (!txtacompanhantes.isEmpty()) {
            message += String.format("Tenho %s acompanhantef. ", txtacompanhantes);
        }

        if (almoco) {
            message += "O passeio deve incluir almoço. ";
        }

        message += String.format("Estou ciente do total de R$ %.2f para este passeio.", valorTotal);

        return message;
    }
}
